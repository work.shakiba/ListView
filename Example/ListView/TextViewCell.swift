//
//  TextViewCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 12/25/1395 AP.
//  Copyright © 1395 mohsen shakiba. All rights reserved.
//

import UIKit
import ListView

class TextViewCell: ListViewCell, UITextViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var textViewLabel: UITextView!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textViewLabel.delegate = self
        textViewLabel.isScrollEnabled = false
        textViewLabel.layer.masksToBounds = false
        textViewLabel.contentInset = UIEdgeInsetsMake(4,-5,0,0);
        self.setBorder(.none)
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.characters.count == 0 {
            placeHolderLabel.isHidden = false
        }else{
            placeHolderLabel.isHidden = true
        }
        self.updateControllerDimentions()
    }
    
    override func getCellHeight() -> CGFloat {
        let sizeOfTextView = textViewLabel.sizeThatFits(textViewLabel.bounds.size)
        return sizeOfTextView.height + 50
    }


}


