//
//  ButtonCell.swift
//  ListView
//
//  Created by mohsen shakiba on 1/1/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import ListView

class ButtonCell: ListViewCell {

    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        self.backgroundColor = .clear
        super.awakeFromNib()
        self.button.isUserInteractionEnabled = false
        
        setBorder(.none)
        
    }

    override func getCellHeight() -> CGFloat {
        return 60
    }
    
}
