//
//  LabelCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 12/19/1395 AP.
//  Copyright © 1395 mohsen shakiba. All rights reserved.
//

import UIKit
import ListView

class LabelCell: ListViewCell {

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label.textAlignment = .center
        self.backgroundColor = .clear
        label.numberOfLines = 99
        label.textColor = .gray
        
        setBorder(.none)
    }
    
    func size(_ top: CGFloat, _ right: CGFloat, _ bottom: CGFloat, _ left: CGFloat) {
        rightConstraint.constant = right
        leftConstraint.constant = left
    }
    
    override func getCellHeight() -> CGFloat {
        var frame = self.label.bounds
        frame.size.height = CGFloat.greatestFiniteMagnitude
        let labelSize = label.sizeThatFits(frame.size)
        return labelSize.height + 25
    }
    
}
