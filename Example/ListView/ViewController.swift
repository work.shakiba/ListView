//
//  ViewController.swift
//  ListView
//
//  Created by mohsen shakiba on 03/02/2017.
//  Copyright (c) 2017 mohsen shakiba. All rights reserved.
//

import UIKit
import ListView

class ViewController: ListViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        section { (s) in
            s.reusableCell(LabelCell.self)
            .setup({ (cell) in
                cell.label.textColor = UIColor.blue
            })
            .click({ (_) in
                let vc = LoginViewController()
                self.present(vc, animated: true, completion: nil)
            })
            .commit()
            
            s.reusableCell(LabelCell.self)
                .setup({ (cell) in
                    cell.label.textColor = UIColor.red
                    cell.label.text = "click to reload"
                })
                .click({ (_) in
                    self.reloadCell("reload")
                })
                .commit("reload")
            
            for _ in 0...15 {
                s.reusableCell(LabelCell.self)
                    .update({ (cell) in
                        cell.label.textColor = UIColor.red
                        cell.label.text = "test"
                    })
                    .commit("reload")
            }
            
            s.cell(LabelCell.self)
            .setup({ (cell) in
                cell.label.text = "click for more"
            })
            .commit()
            
        }
        .commit()
        
    }
    

    override func reachedBottom() {
        print("reacheBottom")
    }
    
    
}

