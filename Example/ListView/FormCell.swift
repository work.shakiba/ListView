//
//  CustomTextCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 12/18/1395 AP.
//  Copyright © 1395 mohsen shakiba. All rights reserved.
//

import UIKit
import ListView

class FormCell: ListViewCell {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.isUserInteractionEnabled = false
        title.textColor = UIColor.gray
    }
    
    override func cellSelected() {
        textField.isUserInteractionEnabled = true
        textField.becomeFirstResponder()
        title.textColor = UIColor.blue
    }
    
    override func cellDeselected() {
        self.textField.isUserInteractionEnabled = false
        title.textColor = UIColor.gray
    }
    
    override func value(_ for: String?) -> Any? {
        return textField.text
    }
    
    override func getCellHeight() -> CGFloat {
        return 50
    }
    
    override func cellValidationFailed(error: String) {
        self.title.textColor = .red
    }

}

