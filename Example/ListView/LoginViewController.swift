//
//  LoginViewController.swift
//  ListView
//
//  Created by mohsen shakiba on 1/1/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import Foundation
import ListView
import Font_Awesome_Swift
import BGTableViewRowActionWithImage

class LoginViewController: ListViewController {
    
    var codeSent = false
    var phone: String?
    
    deinit {
        print("deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        cell(LabelCell.self)
            .update({ (cell) in
                cell.label.text = self.codeSent ? "A verification code was sent to your phone please enter the digits below" : "Please enter your phone to continue"
                
            })
            //            .visiblity({ () -> Bool in
            //                return !self.codeSent
            //            })
            .commit("main")
        
        cell(LabelCell.self)
            .update({ (cell) in
                cell.label.text = self.phone
            })
            .visiblity({ () -> Bool in
                return self.codeSent
            })
            
            .commit()
        
        cell(ButtonCell.self)
            .update({ (cell) in
                cell.button.setTitle("Edit Phone", for: .normal)
            })
            .click({ (cell) in
                self.getPhone()
            })
            .visiblity({ () -> Bool in
                return self.codeSent
            })
            .commit()
        
        section { (s) in
            s.cell(FormCell.self)
                .update({ (cell) in
                    cell.title.text = "Phone"
                    cell.textField.placeholder = "Enter your phone"
                    cell.setBorder(.all)
                })
                .addValidation(StringValidation())
                .config( { provider in
                    let editingAction = UITableViewRowAction(style: .default, title: "remove", handler: { (_, _) in
                        print("swiped")
                    })
                    provider.editingActions.append(editingAction)
                })
                .commit("phone")
            }
            .visiblity({ () -> Bool in
                return !self.codeSent
            })
            .finalize()
        
        section { (s) in
            s.cell(FormCell.self)
                .update({ (cell) in
                    cell.title.text = "Verification Code"
                    cell.textField.placeholder = "4 digit code"
                    cell.setBorder(.all)
                    
                })
                .commit()
            }
            .visiblity({ () -> Bool in
                return self.codeSent
            })
            .finalize("codeee")
        
        cell(ButtonCell.self)
            .update({ (cell) in
                cell.button.setTitle("Send Code", for: .normal)
            })
            .click({ (cell) in
                self.sendCode()
            })
            .commit()
        
        section{ (s) in
            s.cell(LabelCell.self)
                .update({ (cell) in
                    cell.label.text = "show"
                })
                .commit()
            }
            .visiblity({ () -> Bool in
                return !self.codeSent
            })
            .finalize()
        
        
        cell(LabelCell.self)
            .update { (cell) in
                cell.label.text = "click to toggle"
            }
            .click({ (cell) in
                self.codeSent = !self.codeSent
                self.visibilityIfNeeded()
            })
            .commit()
        
        section { (s) in
            s.cell(LabelCell.self)
                .update({ (cell) in
                    cell.label.text = "hide"
                })
                .commit()
            }
            .visiblity({ () -> Bool in
                return self.codeSent
            })
            .commit("hidden")
        
        cell(LabelCell.self)
            .update({ (cell) in
                cell.label.text = "dismiss"
            })
            .click({ (cell) in
                self.dismiss(animated: true, completion: nil)
            })
            .commit()
        
        cell(LabelCell.self)
            .update({ (cell) in
                cell.label.text = "click me to reveal the uncommited cell"
            })
            .click({ (cell) in
                self.commitCellsForAllSectionsIfNeeded()
            })
            .commit()
    
        cell(LabelCell.self)
            .update({ (cell) in
                cell.label.text = "I was precommited cell"
            })
            .preCommit()
    
    }
    
    func sendCode() {
        //        let (result, phone) = valueByValidating("phone")
        //        if !result {
        //            return
        //        }
        //        self.phone = phone
        //        self.codeSent = true
        //        self.visibilityIfNeeded()
    }
    
    func getPhone() {
        self.phone = nil
        self.codeSent = false
        visibilityIfNeeded()
    }
    
}

class StringValidation: ValidationRuleProtocol {
    
    func error(str: String?) -> String? {
        return "error"
    }
    
}
