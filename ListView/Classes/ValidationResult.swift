//
//  ValidationResult.swift
//  Pods
//
//  Created by mohsen shakiba on 1/13/1396 AP.
//
//

import Foundation

public class ValidationResult {
    
    public var success: Bool = false
    public var results: Dictionary<String, String?> = [:]
    
    public func value(key: String) -> String? {
        return results[key] ?? nil
    }
    
    func addValue(key: String, value: String?) {
        results[key] = value
    }
    
}
