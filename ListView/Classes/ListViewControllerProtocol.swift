//
//  ListViewControllerProtocol.swift
//  Pods
//
//  Created by mohsen shakiba on 1/14/1396 AP.
//
//

import Foundation
import UIKit

protocol ListViewControllerProtocol: class {
    
    var tableView: UITableView! { get set }
    
    var view: UIView! { get set }
    
    func commitSection(section: SectionProvider, at: Int?)
    
}
