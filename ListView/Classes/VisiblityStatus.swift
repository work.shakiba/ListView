//
//  VisiblityStatus.swift
//  Pods
//
//  Created by mohsen shakiba on 1/13/1396 AP.
//
//

import Foundation

enum VisibilityStatus {
    
    case visible
    case hidden
    case changeToVisible
    case changeToHidden
    
}
