//
//  CoreListViewCell.swift
//  Pods
//
//  Created by mohsen shakiba on 12/16/1395 AP.
//
//

import Foundation
import UIKit

public class CellProvider<T>: CoreCellProvider {
    
    // set the setup handler for this cell
    // this method is called only once for each cell
    public func setup(_ handler: @escaping (T) -> Void) -> CellProvider<T> {
        let handler: (ListViewCell) -> Void = { cell in
            handler(cell as! T)
        }
        self.setupHandler = handler
        return self
    }

    public func config(_ handler: @escaping (CellProvider<T>) -> Void) ->CellProvider<T> {
        handler(self)
        return self
    }
    
    // set the update handler for this cell
    // this handler is called for every time the cell appears in the list view
    public func update(_ handler: @escaping (T) -> Void) -> CellProvider<T> {
            let handler: (ListViewCell) -> Void = { cell in
                handler(cell as! T)
            }
        self.updateHandler = handler
        return self
    }
    
    // set the click handler for this cell
    public func click(_ handler: @escaping (T) -> Void) -> CellProvider<T> {
        let specificHandler: (ListViewCell) -> Void = { cell in
            handler(cell as! T)
        }
        self.onClickHandler = specificHandler
        return self
    }
    
    // set the visibilty handler for this cell
    public func visiblity(_ handler: @escaping () -> Bool) -> CellProvider<T> {
        self.visibilityHandler = handler
        self.lastVisibilityState = handler()
        return self
    }
    
    // append validation for the given cell
    public func addValidation(_ rule: ValidationRuleProtocol) -> CellProvider<T> {
        self.validationsRules.append(rule)
        return self
    }
    
    // set the estimeted height for cell
    public func setHeight(_ height: CGFloat) -> CellProvider<T> {
        self.estimatedHeight = height
        return self
    }
    
    // will append the cell in the section
    public func commit(_ id: String? = nil, at: Int? = nil) {
        if let id = id {
            self.id = id
        }
        section?.commitCell(self, at: at)
    }
    
    // precommit cell to be commited later
    // while the cell is not commited it's not presented in table view
    public func preCommit(_ id: String? = nil, at: Int? = nil) {
        if let id = id {
            self.id = id
        }
        section?.preCommitCell(self, at: at)
    }
    
    // validation result for cell
    public func value(_ key: String? = nil) -> (Bool, String?) {
        return self.valueByValidating(key)
    }
    
}



