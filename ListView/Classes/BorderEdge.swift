//
//  BorderEdge.swift
//  Pods
//
//  Created by mohsen shakiba on 1/13/1396 AP.
//
//

import Foundation

public enum BorderEdge {
    
    case none
    case bottom
    case top
    case all
    
}

