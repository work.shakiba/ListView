//
//  CoreCellProvider.swift
//  Pods
//
//  Created by mohsen shakiba on 1/22/1396 AP.
//
//

import Foundation

/**
 * the cell is used internally by the library to manage the creating and configuration of the cells
 **/

public class CoreCellProvider {
    
    // identifier for reusing this cell
    public let reuseIdentifier: String
    
    // identifier used for finding this cell
    public var id: String = String.randomString(length: 5)
    
    // UITableViewCell will be assigned to this property once the cell is rendered in the table view
    public weak var cell: ListViewCell?
    
    // weak refrenced pointer to section
    weak var section: SectionProvider?
    
    // handler called only once for the cell
    // the handler is then removed once the table view is distroyed
    var setupHandler: ((ListViewCell) -> Void)?
    
    // handler defined by the user to customize the UITableViewCell
    // this handler will be called once the view is ready to be rendered in the table view and called subsequently each time it is displayed
    // the handler is then removed once the table view is distroyed
    var updateHandler: ((ListViewCell) -> Void)?
    
    // handler added by the user for when the cell is clicked
    // the handler is then removed once the table view is distroyed
    var onClickHandler: ((ListViewCell) -> Void)?
    
    // table view actions
    public var editingActions: [UITableViewRowAction] = []
    
    // handler called every time the cell is about to show to determine if the cell should be displayed or not
    // the handler is then removed once the table view is distroyed
    var visibilityHandler: (() -> Bool)?
    var lastVisibilityState: Bool = true
    
    // rules applied to this cell
    var validationsRules: [ValidationRuleProtocol] = []
    
    // used for when the cell is not dequeued yet
    var estimatedHeight: CGFloat = UITableViewAutomaticDimension
    
    // if cell is commited and added to table view
    var commited = false
    
    // default initializer
    public init(section: SectionProvider, reuseIdentifier: String) {
        self.section = section
        self.reuseIdentifier = reuseIdentifier
    }
    
    // if cell visilble or not
    // without changing the currentVisbilityState of cell
    func isVisible() -> Bool {
        return visibilityHandler?() ?? true
    }
    
    // returns current visibility status of cell
    func visibilityStatus() -> VisibilityStatus {
        var status: VisibilityStatus = .visible
        let currentVisbilityState = visibilityHandler?() ?? true
        if lastVisibilityState != currentVisbilityState {
            if lastVisibilityState == false {
                status = .changeToVisible
            }else{
                status = .changeToHidden
            }
        }else {
            if lastVisibilityState == true {
                status = .visible
            }else{
                status = .hidden
            }
        }
        lastVisibilityState = currentVisbilityState
        return status
    }
    
    // returns the validation result for cell
    func valueByValidating(_ forKey: String? = nil) -> (success: Bool, result: String?) {
        let value = cell?.value(forKey) as? String
        for rule in validationsRules {
            if let error = rule.error(str: value ) {
                cell?.cellValidationFailed(error: error)
                return (false, value)
            }
        }
        cell?.cellValidationSuccess()
        return (true, value)
    }
    
    // clear all references of this cell
    func dispose() {
        self.onClickHandler = nil
        self.setupHandler = nil
        self.visibilityHandler = nil
        self.updateHandler = nil
    }
    
}

