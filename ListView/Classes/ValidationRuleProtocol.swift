//
//  ValidationRuleProtocol.swift
//  Pods
//
//  Created by mohsen shakiba on 1/13/1396 AP.
//
//

import Foundation

public protocol ValidationRuleProtocol {
    
    /// returns nil if the validation succeeds and the error message if the the validation fails
    func error(str: String?) -> String?
    
}
