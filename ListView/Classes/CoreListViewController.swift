//
//  ListViewSection.swift
//  Pods
//
//  Created by mohsen shakiba on 12/12/1395 AP.
//
//

import UIKit
import Foundation

/**
 * the controller in charge of managing the table view and providing interface for managing cells and sections
 **/

open class CoreListViewController: UIViewController {
    
    
    public var tableView: UITableView!
    
    private var _sections: [SectionProvider] = []
    
    public var containerTopConstraint: NSLayoutConstraint!
    public var containerBottomConstraint: NSLayoutConstraint!
    public var containerLeftConstraint: NSLayoutConstraint!
    public var containerRightConstraint: NSLayoutConstraint!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    
    open func setupTableView() {
        self.tableView = UITableView(frame: self.view.bounds, style: .grouped)
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        tableView.dataSource = self
        self.view.addSubview(tableView)
        addConstraints()
    }
    
    open func addConstraints() {
        containerTopConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: topLayoutGuide, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        containerBottomConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: bottomLayoutGuide, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        containerLeftConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0)
        containerRightConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0)
        view.addConstraints([containerTopConstraint, containerBottomConstraint, containerLeftConstraint, containerRightConstraint])
    }
    
    
    public func reload() {
        self.tableView.reloadData()
    }
    
    func sectionByIndex(_ sectionIndex: Int) -> SectionProvider {
        let section = visibleSections()[sectionIndex]
        return section
    }
    
    func visibleSections() -> [SectionProvider] {
        return _sections.filter({$0.isVisible()})
    }
    
    func allSections() -> [SectionProvider] {
        return _sections
    }
    
    func insertSection(_ section: SectionProvider, at: Int) {
        self._sections.insert(section, at: at)
    }
    
    func deleteSection(_ id: String) {
        if let index = self._sections.index(where: {$0.id == id}) {
            let section = self._sections[index]
            section.dispose()
            self._sections.remove(at: index)
            let indexSet = NSIndexSet(index: index) as IndexSet
            self.tableView.deleteSections(indexSet, with: .fade)
        }
    }
    
    func didReachBottom(of section: SectionProvider) {
        
    }
    
}

extension CoreListViewController: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return visibleSections().count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sectionByIndex(indexPath.section)
        let reuseIdentifier = section.cellReuseIdentifierBy(row: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier!) as! ListViewCell
        section.cellDequeued(row: indexPath.row, cell: cell)
        return cell
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sectionByIndex(section)
        return section.visibleCellCount()
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = sectionByIndex(indexPath.section)
        return section.cellHeightBy(row: indexPath.row)
    }
    
}

extension CoreListViewController: UITableViewDelegate {
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        section.cellSelectedBy(row: indexPath.row)
    }
    
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        section.cellDeselectedBy(row: indexPath.row)

    }
    
    
    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        section.cellHeighlightedBy(row: indexPath.row)
    }
    
    
    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        section.cellUnhighlightedBy(row: indexPath.row)
    }
    
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        section.cellWillDispalayByRow(row: indexPath.row)
        if section.didReachBottom(row: indexPath.row) {
            self.didReachBottom(of: section)
        }
    }
    
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = sectionByIndex(section)
        return section.getHeaderView()
    }
    
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let section = sectionByIndex(section)
        return section.getFooterView()
    }
    
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = sectionByIndex(section)
        return section.getHeaderHeight()
    }
    
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let section = sectionByIndex(section)
        return section.getFooterHeight()
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let section = sectionByIndex(indexPath.section)
        let coreProvider = section.visibleCellBy(row: indexPath.row)
        return coreProvider.editingActions
    }
    
}




