//
//  ListViewSection.swift
//  Pods
//
//  Created by mohsen shakiba on 12/12/1395 AP.
//
//

import Foundation


open class SectionProvider {
    
    // section identifier
    public var id: String
    
    // index of this section in table view
    var index: Int? = nil
    
    // weak referenced pointer to controller
    weak var controller: ListViewControllerProtocol?
    
    // array of cell providers
    internal var cellProviders: [CoreCellProvider] = []
    
    // used to determine next status for section
    internal var lastVisibilityState: Bool = true
    var visibilityHandler: (() -> Bool)?
    
    // config handler
    // this handler is called once the section is finalized
    // once handler is called, it is removed
    var configHandler: ((SectionProvider) -> Void)?
    
    // header and footer height
    public var footerHeight: CGFloat = 0.1
    public var headerHeight: CGFloat = 0.1
    
    // header and footer view
    public var footerView: UIView?
    public var headerView: UIView?
    
    // reached bottom status
    var reachedBottomCellIndex = 0
    
    
    // default initializer
    init(id: String, controller: ListViewControllerProtocol) {
        self.id = id
        self.controller = controller
    }
    
    // get commited providers
    func commitedProviders() -> [CoreCellProvider] {
        return self.cellProviders.filter({$0.commited == true})
    }
    
    // get un-commited providers
    func unCommitedProviders() -> [CoreCellProvider] {
        return self.cellProviders.filter({$0.commited == false})
    }
    
    // find visible cell by row
    func visibleCellBy(row: Int) -> CoreCellProvider {
        return visibleProviders()[row]
    }
    
    // count of all providers
    func allProviderCount() -> Int {
        return allProviders().count
    }
    
    // count of all providers
    func commitedProvidersCount() -> Int {
        return commitedProviders().count
    }
    
    // count of all visible cells
    func visibleCellCount() -> Int {
        return visibleProviders().count
    }
    
    // index of visible cell by id
    func visibleCellIndex(id: String) -> Int? {
        for (i, cell) in visibleProviders().enumerated() {
            if cell.id == id {
                return i
            }
        }
        return nil
    }
    
    // will remove all the hidden cells and add all the visible cells
    // if the section is hidden this method will have no effect
    // if reload cell is true then cells that are currenlty visible, will be reloaded
    func visibilityIfNeeded(reloadCells: Bool = false) {
        guard let sectionIndex = self.index else {
            return
        }
        commitedProviders().forEach { (provider) -> Void in
            let cellIndex = self.visibleProviders().index(where: {$0.id == provider.id}) ?? -1
            let visiblityStatus = provider.visibilityStatus()
            if visiblityStatus == .changeToVisible {
                let indexPath = IndexPath(item: cellIndex, section: sectionIndex)
                self.controller?.tableView.insertRows(at: [indexPath], with: .fade)
            }else if visiblityStatus == .changeToHidden {
                let indexPath = IndexPath(item: provider.cell!.indexPath.row, section: sectionIndex)
                self.controller?.tableView.deleteRows(at: [indexPath], with: .fade)
            }else if visiblityStatus == .visible {
                if reloadCells {
                    let indexPath = IndexPath(item: provider.cell!.indexPath.row, section: sectionIndex)
                    self.controller?.tableView.reloadRows(at: [indexPath], with: .fade)
                }
            }
        }
    }
    
    // current visibllity status of section
    func visibilityStatus(_ set: Bool = false) -> VisibilityStatus {
        var status: VisibilityStatus = .visible
        let currentVisbilityState = visibilityHandler?() ?? true
        if lastVisibilityState != currentVisbilityState {
            if lastVisibilityState == false {
                status = .changeToVisible
            }else{
                status = .changeToHidden
            }
        }else {
            if lastVisibilityState == true {
                status = .visible
            }else{
                status = .hidden
            }
        }
        if set {
            lastVisibilityState = currentVisbilityState
        }
        return status
    }
    
    // current visibllity status of section wihtout changing the lastVisibilityState
    func isVisible() -> Bool {
        return self.visibilityHandler?() ?? true
    }
    
    // cell reuseIdentifier
    func cellReuseIdentifierBy(row: Int) -> String? {
        return visibleProviders()[row].reuseIdentifier
    }
    
    // when cell dequeued
    func cellDequeued(row: Int, cell: ListViewCell) {
        let provider = visibleProviders()[row]
        provider.section = self
        provider.cell = cell
        provider.cell?.provider = provider
        cell.indexPath = IndexPath(row: row, section: self.index!)
        if cell.needsSetup {
            cell.needsSetup = false
            if let setupHandler = provider.setupHandler {
                setupHandler(cell)
            }
        }
        
        provider.updateHandler?(cell)
        provider.cell?.updateBorder()
        cell.cellConfigFinished()
    }
    
    // when cell is selected
    func cellSelectedBy(row: Int) {
        let provider = visibleProviders()[row]
        provider.cell?.cellSelected()
        provider.onClickHandler?(provider.cell!)
    }
    
    // when cell deselected
    func cellDeselectedBy(row: Int) {
        let core = visibleProviders()[row]
        core.cell?.cellDeselected()
    }
    
    // when cell highlighted
    func cellHeighlightedBy(row: Int) {
        let provider = visibleProviders()[row]
        provider.cell?.cellHighlighted()
    }
    
    // when cell un highlighted
    func cellUnhighlightedBy(row: Int) {
        let provider = visibleProviders()[row]
        provider.cell?.cellUnhighlighted()
    }
    
    // when cell will display
    func cellWillDispalayByRow(row: Int) {
        let provider = visibleProviders()[row]
        provider.cell?.cellWillDisplay()
        provider.cell?.updateBorder()
    }
    
    // get cell height
    func cellHeightBy(row: Int) -> CGFloat {
        let provider = visibleProviders()[row]
        if provider.cell == nil {
            return provider.estimatedHeight
        }
        
        return provider.cell!.getCellHeight()
    }
    
    // section header height
    func getHeaderHeight() -> CGFloat {
        if visibleCellCount() == 0 {
            return 0.1
        }
        return self.headerHeight
    }
    
    // section footer height
    func getFooterHeight() -> CGFloat {
        if visibleCellCount() == 0 {
            return 0.1
        }
        return self.footerHeight
    }
    
    // heade view
    func getHeaderView() -> UIView? {
        return headerView
    }
    
    // footer view
    func getFooterView() -> UIView? {
        return footerView
    }
    
    // clear all references for this section
    func dispose() {
        self.visibilityHandler = nil
        self.configHandler = nil
        for provider in allProviders() {
            provider.dispose()
        }
    }
    
    // find provider by id
    func coreCellProviderBy(id: String) -> CoreCellProvider? {
        return allProviders().first(where: {$0.id == id})
    }
    
    // if row is the last row in section
    func didReachBottom(row: Int) -> Bool {
        let visibleSectionCount = visibleCellCount()
        let lastCellIndex = visibleSectionCount - 1
        if lastCellIndex <= reachedBottomCellIndex {
            return false
        }
        if row == visibleSectionCount - 1 {
            reachedBottomCellIndex = row
            return true
        }
        return false
    }
    
}

extension SectionProvider {
    
    // finalize the section
    public func finalize(_ id: String? = nil, at: Int? = nil) {
        let id = id ?? String.randomString(length: 5)
        self.id = id
        self.commitCellsIfNeeded()
        self.controller?.commitSection(section: self, at: at)
    }
    
    // duplicate of finalize method
    public func commit(_ id: String? = nil, at: Int? = nil) {
        self.finalize(id, at: at)
    }
    
    // find provider by id
    public func cellProviderBy<T>(id: String) -> CellProvider<T>? {
        return allProviders().first(where: {$0.id == id}) as? CellProvider<T>
    }
    
    // find cell by id
    public func cellBy<T>(id: String) -> T? {
        let provider: CellProvider<T>? = cellProviderBy(id: id)
        return provider?.cell as? T
    }
    
    // set visiblity handler for section
    public func visiblity(_ handler: @escaping () -> Bool) -> SectionProvider {
        self.visibilityHandler = handler
        self.lastVisibilityState = handler()
        return self
    }
    
    // cell with unique reuse identifier
    // new cell is created for each cell
    public func cell<T>(_ type: T.Type) -> CellProvider<T> where T: ListViewCell {
        let nib = UINib.fromType(type: T.self)
        let nibName = UINib.name(type: T.self) + String.randomString(length: 5)
        controller?.tableView.register(nib, forCellReuseIdentifier: nibName)
        let provider = CellProvider<T>(section: self, reuseIdentifier: nibName)
        return provider
    }
    
    // cell with reuse identifier
    // this cell will be reused for the same types
    public func reusableCell<T>(_ type: T.Type) -> CellProvider<T> where T: ListViewCell {
        let nib = UINib.fromType(type: T.self)
        let nibName = UINib.name(type: T.self)
        controller?.tableView.register(nib, forCellReuseIdentifier: nibName)
        let provider = CellProvider<T>(section: self, reuseIdentifier: nibName)
        return provider
    }
    
    public func preCommitCells(_ cells: [CoreCellProvider], inRange: CountableClosedRange<Int>? = nil) {
        if cells.count == 0 { return }
        let range = inRange ?? (self.allProviderCount())...(self.allProviderCount() + cells.count - 1)
        for i in range {
            let cellIndex = i - range.first!
            let provider = cells[cellIndex]
            self.insert(provider: provider, at: i)
        }
    }
    
    // commit cell
    public func preCommitCell(_ provider: CoreCellProvider, at: Int? = nil) {
        let at = at ?? self.allProviderCount()
        self.preCommitCells([provider], inRange: at...at)
    }
    
    public func commitCellsIfNeeded() {
        let uncommitedProviders = unCommitedProviders() 
        controller?.tableView.beginUpdates()
        var newRows = [IndexPath]()
        for provider in uncommitedProviders {
            provider.commited = true
            if provider.isVisible() {
                guard let sectionIndex = self.index else { continue }
                if let cellIndex = visibleCellIndex(id: provider.id) {
                    let indexPath = IndexPath(item: cellIndex, section: sectionIndex)
                    newRows.append(indexPath)
                }
            }
        }
        controller?.tableView.insertRows(at: newRows, with: .fade)
        controller?.tableView.endUpdates()
    }
    
    public func commitCellsIfNeededWithouthAnimation() {
        UIView.setAnimationsEnabled(false)
        commitCellsIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    // commit many cells
    public func commitCells(_ cells: [CoreCellProvider], inRange: CountableClosedRange<Int>? = nil) {
        self.preCommitCells(cells, inRange: inRange)
        self.commitCellsIfNeeded()
    }
    
    // commit cell
    public func commitCell(_ provider: CoreCellProvider, at: Int? = nil) {
        let at = at ?? self.allProviderCount()
        self.commitCells([provider], inRange: at...at)
    }
    
    // returns the validation result for all cells in section
    public func validate() -> ValidationResult {
        let result = ValidationResult()
        result.success = true
        for provider in commitedProviders() {
            let (success, value) = provider.valueByValidating()
            if !success {
                result.success = false
            }
            result.addValue(key: provider.id, value: value)
        }
        return result
    }
    
    // remove all cells
    public func removeAllCells() {
        self.clearProviders()
        guard let sectionIndex = self.index else { return }
        let indexSet = NSIndexSet(index: sectionIndex)
        self.controller?.tableView.reloadSections(indexSet as IndexSet, with: .none)
    }
    
    // return visible providers
    public func visibleProviders() -> [CoreCellProvider] {
        let activeRows = commitedProviders().filter { (row) -> Bool in
            return row.isVisible()
        }
        return activeRows
    }
    
    // insert provider
    public func insert(provider: CoreCellProvider, at: Int) {
        self.cellProviders.insert(provider, at: at)
    }
    
    // clear list of providers
    public func clearProviders() {
        self.cellProviders.removeAll()
    }
    
    // get all providers
    public func allProviders() -> [CoreCellProvider] {
        return self.cellProviders
    }
    
}



