//
//  ListViewRow.swift
//  Pods
//
//  Created by mohsen shakiba on 12/12/1395 AP.
//
//

import Foundation
import UIKit

/** 
 * base class for the cells
 * each cell that is going to be used in this library must implement this base class
 **/

open class ListViewCell: UITableViewCell {
    
    public var id: String!
    public var indexPath: IndexPath!
    weak var provider: CoreCellProvider?
    
    open var borderColor = UIColor(white: 0, alpha: 0.1)
    var borderEdge: BorderEdge = .bottom
    var needsSetup = false
    
    open override func awakeFromNib() {
        needsSetup = true
        super.awakeFromNib()
    }
    
    open override func setSelected(_ selected: Bool, animated: Bool) {
        self.selectionStyle = .none
    }
        
    open func cellWillDisplay() {}
    
    open func cellConfigFinished() {}
    
    open func cellRemoved() {}
    
    open func cellSelected() {}
    
    open func cellDeselected() {}
    
    open func cellHighlighted() {}
    
    open func cellUnhighlighted() {}
    
    open func cellValidationSuccess() {}
    
    open func cellValidationFailed(error: String) {}
    
    open func getCellHeight() -> CGFloat {
        return contentView.bounds.height
    }
    
    open func value(_ for: String? = nil) -> Any? {
        return nil
    }

    public func updateControllerDimentions() {
        provider?.section?.controller?.tableView.beginUpdates()
        provider?.section?.controller?.tableView.endUpdates()
    }
    
    public func setBorder(_ edge: BorderEdge) {
        self.borderEdge = edge
    }
    
    func updateBorder() {
        
        let edge = self.borderEdge
        
        if edge == .top || edge == .all {
            addBorderIfNeeded(.top, color: self.borderColor)
        }
        if edge == .bottom || edge == .all {
            addBorderIfNeeded(.bottom, color: self.borderColor)
        }
        
        if edge == .none {
            self.subviews.forEach({view in
                if view.tag == 1111 {
                    view.removeFromSuperview()
                }
            })
        }
        
    }
        
}





