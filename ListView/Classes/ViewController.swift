////
////  ViewController.swift
////  Pods
////
////  Created by mohsen shakiba on 1/14/1396 AP.
////
////
//
//import Foundation
//import UIKit
//
//open class Controller: UIViewController {
//    
//    
//    public var tableView: UITableView!
//    public var viewManager: ViewManager! {
//        didSet {
//            self.viewManager.tableView = self.tableView
//            self.viewManager.view = self.view
//        }
//    }
//    
//    public var containerTopConstraint: NSLayoutConstraint!
//    public var containerBottomConstraint: NSLayoutConstraint!
//    public var containerLeftConstraint: NSLayoutConstraint!
//    public var containerRightConstraint: NSLayoutConstraint!
//    
//    open override func viewDidLoad() {
//        super.viewDidLoad()
//        setupTableView()
//    }
//    
//    open override func viewWillAppear(_ animated: Bool) {
//        self.viewManager.layoutListView()
//    }
//    
//    
//    open func setupTableView() {
//        self.tableView = UITableView(frame: self.view.bounds, style: .grouped)
//        tableView.delegate = self
//        tableView.translatesAutoresizingMaskIntoConstraints = false
//        tableView.separatorStyle = .none
//        tableView.dataSource = self
//        self.view.addSubview(tableView)
//        addConstraints()
//    }
//    
//    open func addConstraints() {
//        containerTopConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: topLayoutGuide, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
//        containerBottomConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: bottomLayoutGuide, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
//        containerLeftConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0)
//        containerRightConstraint = NSLayoutConstraint(item: tableView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0)
//        view.addConstraints([containerTopConstraint, containerBottomConstraint, containerLeftConstraint, containerRightConstraint])
//    }
//    
//}
//
//extension Controller: UITableViewDataSource {
//    
//    public func numberOfSections(in tableView: UITableView) -> Int {
//        return viewManager.visibleSections().count
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let section = viewManager.sectionByIndex(indexPath.section)
//        let reuseIdentifier = section.cellReuseIdentifierBy(row: indexPath.row)
//        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier!) as! ListViewCell
//        section.cellDequeued(row: indexPath.row, cell: cell)
//        return cell
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let section = viewManager.sectionByIndex(section)
//        return section.visibleCellCount()
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let section = viewManager.sectionByIndex(indexPath.section)
//        return section.cellHeightBy(row: indexPath.row)
//    }
//    
//}
//
//extension Controller: UITableViewDelegate {
//    
//    
//    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let section = viewManager.sectionByIndex(indexPath.section)
//        section.cellSelectedBy(row: indexPath.row)
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let section = viewManager.sectionByIndex(indexPath.section)
//        section.cellDeselectedBy(row: indexPath.row)
//        
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
//        let section = viewManager.sectionByIndex(indexPath.section)
//        section.cellHeighlightedBy(row: indexPath.row)
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
//        let section = viewManager.sectionByIndex(indexPath.section)
//        section.cellUnhighlightedBy(row: indexPath.row)
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let section = viewManager.sectionByIndex(indexPath.section)
//        section.cellWillDispalayByRow(row: indexPath.row)
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let section = viewManager.sectionByIndex(section)
//        return section.getHeaderView()
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let section = viewManager.sectionByIndex(section)
//        return section.getFooterView()
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        let section = viewManager.sectionByIndex(section)
//        return section.getHeaderHeight()
//    }
//    
//    
//    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        let section = viewManager.sectionByIndex(section)
//        return section.getFooterHeight()
//    }
//    
//}
//
//open class ViewManager: NSObject, ListViewControllerProtocol {
//    
//    public var view: UIView!
//    public var tableView: UITableView!
//    private var sections: [SectionProvider] = []
//    
//    // if the keyboard is visible in this controller
//    internal var isKeyboardVsibile: Bool = false
//    
//    func sectionByIndex(_ sectionIndex: Int) -> SectionProvider {
//        let section = visibleSections()[sectionIndex]
//        return section
//    }
//    
//    func visibleSections() -> [SectionProvider] {
//        return sections.filter({$0.isVisible()})
//    }
//    
//    func allSections() -> [SectionProvider] {
//        return sections
//    }
//    
//    func insertSection(_ section: SectionProvider, at: Int) {
//        sections.insert(section, at: at)
//        let indexSet = NSIndexSet(index: at) as IndexSet
//        self.tableView.insertSections(indexSet, with: .fade)
//    }
//    
//    func deleteSection(_ id: String) {
//        if let index = sections.index(where: {$0.id == id}) {
//            let section = sections[index]
//            section.dispose()
//            sections.remove(at: index)
//            let indexSet = NSIndexSet(index: index) as IndexSet
//            self.tableView.deleteSections(indexSet, with: .fade)
//        }
//    }
//    
//    func coreCellProviderBy(_ id: String) -> CoreCellProvider? {
//        for s in visibleSections() {
//            if let cell = s.coreCellProviderBy(id: id) {
//                return cell
//            }
//        }
//        return nil
//    }
//    
//    func listenToKeyboardChange() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
//    }
//    
//    func keyboardWillShow(_ notification: Notification){
//        if isKeyboardVsibile {
//            return
//        }
//        isKeyboardVsibile = true
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            keyboard(visible: self.isKeyboardVsibile, rect: keyboardSize)
//        }
//    }
//    
//    
//    func keyboardWillHide(_ notification: Notification){
//        if !isKeyboardVsibile {
//            return
//        }
//        isKeyboardVsibile = false
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            keyboard(visible: self.isKeyboardVsibile, rect: keyboardSize)
//        }
//    }
//    
//    func visiblity(reload: Bool) {
//        tableView.beginUpdates()
//        for section in allSections() {
//            let status = section.visibilityStatus(true)
//            if status == .changeToHidden {
//                let sectionIndex = section.index!
//                let indexSet = NSIndexSet(index: sectionIndex)
//                self.tableView.deleteSections(indexSet as IndexSet, with: .fade)
//            }
//            if status == .changeToVisible {
//                let index = visibleSections().index(where: {$0.id == section.id})
//                let sectionIndex = index!
//                section.index = index
//                let indexSet = NSIndexSet(index: sectionIndex)
//                self.tableView.insertSections(indexSet as IndexSet, with: .fade)
//            }
//            
//            if status == .visible {
//                section.visibilityIfNeeded(reloadCells: reload)
//            }
//        }
//        self.updateSectionIndex()
//        tableView.endUpdates()
//    }
//    
//    func updateSectionIndex() {
//        var visibleSectionIndex = 0
//        for section in allSections() {
//            let visible = section.visibilityHandler?() ?? true
//            if !visible {
//                section.index = nil
//            }else{
//                section.index = visibleSectionIndex
//                visibleSectionIndex += 1
//            }
//        }
//    }
//    
//    func commitSection(section: SectionProvider, at: Int? = nil) {
//        UIView.setAnimationsEnabled(false)
//        tableView.beginUpdates()
//        let index = at ?? allSections().count
//        insertSection(section, at: index)
//        updateSectionIndex()
//        if let sectionIndex = section.index {
//            let indexSet = NSIndexSet(index: sectionIndex)
//            tableView.insertSections(indexSet as IndexSet, with: UITableViewRowAnimation.fade)
//        }
//        section.configHandler?(section)
//        section.configHandler = nil
//        tableView.endUpdates()
//        UIView.setAnimationsEnabled(true)
//    }
//    
//}
//
//extension ViewManager {
//    
//    open func layoutListView() {
//        
//    }
//    
//    // will force dispose all sections
//    public func dispose() {
//        for section in allSections() {
//            section.dispose()
//        }
//    }
//    
//    // find cell provider by id
//    public func cellProviderBy<T>(_ id: String) -> CellProvider<T>? {
//        for s in visibleSections() {
//            if let cell: CellProvider<T> = s.cellProviderBy(id: id) {
//                return cell
//            }
//        }
//        return nil
//    }
//    
//    // find cell by id
//    public func cellBy<T>(_ id: String) -> T {
//        for s in visibleSections() {
//            if let provider: CellProvider<T> = s.cellProviderBy(id: id) {
//                return provider.cell as! T
//            }
//        }
//        fatalError("cell not found")
//    }
//    
//    // value after validation of cell
//    public func valueForCellBy(_ id: String, _ key: String? = nil) -> String? {
//        let (_, value) = coreCellProviderBy(id)?.valueByValidating() ?? (false, nil)
//        return value
//    }
//    
//    // validation result of cell
//    public func validationResultForCellBy(_ id: String, _ key: String? = nil) -> (Bool, String?) {
//        let (result, value) = coreCellProviderBy(id)?.valueByValidating() ?? (false, nil)
//        return (result, value)
//    }
//    
//    // returns index path of cell by id
//    public func cellIndexPathBy(_ id: String) -> IndexPath? {
//        for (i, s) in visibleSections().enumerated() {
//            if let cellIndex = s.visibleCellIndex(id: id) {
//                return IndexPath(row: cellIndex, section: i)
//            }
//        }
//        return nil
//    }
//    
//    // returns index of section by id
//    public func sectionIndexBy(_ id: String) -> Int? {
//        for (i, s) in visibleSections().enumerated() {
//            if s.id == id {
//                return i
//            }
//        }
//        return nil
//    }
//    
//    // returns section provider by id
//    public func sectionBy(_ id: String) -> SectionProvider? {
//        return allSections().first(where: { $0.id == id })
//    }
//    
//    // reload cell by the given id
//    // the update handler will be called after the update
//    public func reloadCell(_ id: String){
//        self.visibilityIfNeeded()
//        if let indexPath = cellIndexPathBy(id) {
//            tableView.reloadRows(at: [indexPath], with: .fade)
//        }
//    }
//    
//    // will rearrange all cells and hide or show according to the visibility handler
//    public func visibilityIfNeeded() {
//        self.visiblity(reload: false)
//    }
//    
//    // will rearrange all cells and hide or show according to the visibility handler
//    // also will reload all cells that are not changed
//    public func reloadIfNeeded(_ reload: Bool = false) {
//        self.visiblity(reload: true)
//    }
//    
//    // create new section provider and pass in closure
//    public func section(_ configHandler: ((SectionProvider) -> Void)? = nil ) -> SectionProvider {
//        let id = String.randomString(length: 5)
//        let listViewSection = SectionProvider(id: id, controller: self)
//        listViewSection.configHandler = configHandler
//        return listViewSection
//    }
//    
//    // create new section and insert new cell
//    public func cell<T>(_ type: T.Type) -> CellProvider<T> where T: ListViewCell {
//        var cell: CellProvider<T>!
//        section { (s) in
//            cell = s.cell(type)
//            }
//            .finalize()
//        return cell
//    }
//    
//    // reload section by id
//    public func reloadSectionBy(_ id: String) {
//        visibilityIfNeeded()
//        if let sectionIndex = sectionIndexBy(id) {
//            let indexSet = NSIndexSet(index: sectionIndex) as IndexSet
//            self.tableView.reloadSections(indexSet, with: .fade)
//        }
//    }
//    
//    // remove and dispose the section by id
//    public func removeSectionBy(id: String) {
//        visibilityIfNeeded()
//        deleteSection(id)
//    }
//    
//    // override to change the behaviour of table view when the keyboard is vaibile
//    open func keyboard(visible: Bool, rect: CGRect) {
//        if visible {
//            tableView.contentInset.bottom +=  rect.height + 50
//        }else{
//            tableView.contentInset.bottom -= rect.height + 50
//        }
//    }
//    
//}
//
//
