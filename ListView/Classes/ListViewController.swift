//
//  ListViewController.swift
//  Pods
//
//  Created by mohsen shakiba on 12/12/1395 AP.
//
//

import Foundation
import UIKit

/**
 * the class used by view controllers to implement the list view
 **/

open class ListViewController: CoreListViewController, ListViewControllerProtocol {
    
    // indentifier for this controller
    public var id = String.randomString(length: 10)
    
    public var preventControllerFromClearingReferences = false
    
    // if the keyboard is visible in this controller
    internal var isKeyboardVsibile: Bool = false
    
    
    // view did load
    open override func viewDidLoad() {
        super.viewDidLoad()
        listenToKeyboardChange()
    }
    
    // view did disappear
    // this method is used to dispose all sections and cells
    open override func viewDidDisappear(_ animated: Bool) {
        if preventControllerFromClearingReferences { return }
        super.viewDidDisappear(animated)
        var found = false
        self.navigationController?.viewControllers.forEach({ (controller) in
            if let controller = controller as? ListViewController {
                if controller.id == self.id {
                    found = true
                }
            }
        })
        if !found {
            self.dispose()
        }
    }
    
    // will force dispose all sections
    public func dispose() {
        for section in allSections() {
            section.dispose()
        }
    }
    
    // find cell provider by id
    public func cellProviderBy<T>(_ id: String) -> CellProvider<T>? {
        for s in visibleSections() {
            if let cell: CellProvider<T> = s.cellProviderBy(id: id) {
                return cell
            }
        }
        return nil
    }
    
    // find cell by id
    public func cellBy<T>(_ id: String) -> T? {
        for s in visibleSections() {
            if let provider = s.coreCellProviderBy(id: id) {
                return provider.cell as? T
            }
        }
        return nil
    }
    
    // value after validation of cell
    public func valueForCellBy(_ id: String, _ key: String? = nil) -> String? {
        let (_, value) = coreCellProviderBy(id)?.valueByValidating() ?? (false, nil)
        return value
    }
    
    // validation result of cell
    public func validationResultForCellBy(_ id: String, _ key: String? = nil) -> (Bool, String?) {
        let (result, value) = coreCellProviderBy(id)?.valueByValidating() ?? (false, nil)
        return (result, value)
    }
    
    // returns index path of cell by id
    public func cellIndexPathBy(_ id: String) -> IndexPath? {
        for (i, s) in visibleSections().enumerated() {
            if let cellIndex = s.visibleCellIndex(id: id) {
                return IndexPath(row: cellIndex, section: i)
            }
        }
        return nil
    }
    
    // returns index of section by id
    public func sectionIndexBy(_ id: String) -> Int? {
        for (i, s) in visibleSections().enumerated() {
            if s.id == id {
                return i
            }
        }
        return nil
    }
    
    // returns section provider by id
    public func sectionBy(_ id: String) -> SectionProvider? {
        return allSections().first(where: { $0.id == id })
    }
    
    // reload cell by the given id
    // the update handler will be called after the update
    public func reloadCell(_ id: String){
        self.visibilityIfNeeded()
        if let indexPath = cellIndexPathBy(id) {
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    // will rearrange all cells and hide or show according to the visibility handler
    public func visibilityIfNeeded() {
        self.visiblity(reload: false)
    }
    
    // will rearrange all cells and hide or show according to the visibility handler
    // also will reload all cells that are not changed
    public func reloadIfNeeded(_ reload: Bool = false) {
        self.visiblity(reload: true)
    }
    
    // create new section provider and pass in closure
    public func section(_ configHandler: ((SectionProvider) -> Void)? = nil ) -> SectionProvider {
        let id = String.randomString(length: 5)
        let listViewSection = SectionProvider(id: id, controller: self)
        listViewSection.configHandler = configHandler
        return listViewSection
    }
    
    // create new section and insert new cell
    public func cell<T>(_ type: T.Type) -> CellProvider<T> where T: ListViewCell {
        var cell: CellProvider<T>!
        section { (s) in
            cell = s.cell(type)
        }
        .finalize()
        return cell
    }
    
    // reload section by id
    public func reloadSectionBy(_ id: String) {
        visibilityIfNeeded()
        if let sectionIndex = sectionIndexBy(id) {
            let indexSet = NSIndexSet(index: sectionIndex) as IndexSet
            self.tableView.reloadSections(indexSet, with: .fade)
        }
    }
    
    // remove and dispose the section by id
    public func removeSectionBy(id: String) {
        visibilityIfNeeded()
        deleteSection(id)
    }
    
    // commit all un-commited cells in all sections
    public func commitCellsForAllSectionsIfNeeded() {
        for section in visibleSections() {
            section.commitCellsIfNeeded()
        }
    }
    
    // override to change the behaviour of table view when the keyboard is vaibile 
    open func keyboard(visible: Bool, rect: CGRect) {
        if visible {
            tableView.contentInset.bottom +=  rect.height + 50
        }else{
            tableView.contentInset.bottom -= rect.height + 50
        }
    }
    
    override func didReachBottom(of section: SectionProvider) {
        reachedEnd(of: section)
        if let lastSection = self.visibleSections().last {
            if section.id == lastSection.id {
                self.reachedBottom()
            }
        }
    }
    
    // called when table view is reached end of section
    open func reachedEnd(of section: SectionProvider) {
        
    }
    
    // called when table view is reached end of table view
    open func reachedBottom() {
        
    }
    
    // returns number of sections in table view
    public func sectionCount() -> Int {
        return visibleSections().count
    }
    
}

extension ListViewController {
    
    // find cell provider by id
    func coreCellProviderBy(_ id: String) -> CoreCellProvider? {
        for s in visibleSections() {
            if let cell = s.coreCellProviderBy(id: id) {
                return cell
            }
        }
        return nil
    }
    
    func listenToKeyboardChange() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    func keyboardWillShow(_ notification: Notification){
        if isKeyboardVsibile {
            return
        }
        isKeyboardVsibile = true
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboard(visible: self.isKeyboardVsibile, rect: keyboardSize)
        }
    }
    
    
    func keyboardWillHide(_ notification: Notification){
        if !isKeyboardVsibile {
            return
        }
        isKeyboardVsibile = false
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboard(visible: self.isKeyboardVsibile, rect: keyboardSize)
        }
    }
    
    func visiblity(reload: Bool) {
        tableView.beginUpdates()
        for section in allSections() {
            let status = section.visibilityStatus(true)
            if status == .changeToHidden {
                let sectionIndex = section.index!
                let indexSet = NSIndexSet(index: sectionIndex)
                self.tableView.deleteSections(indexSet as IndexSet, with: .fade)
            }
            if status == .changeToVisible {
                let index = visibleSections().index(where: {$0.id == section.id})
                let sectionIndex = index!
                section.index = index
                let indexSet = NSIndexSet(index: sectionIndex)
                self.tableView.insertSections(indexSet as IndexSet, with: .fade)
            }
            
            if status == .visible {
                section.visibilityIfNeeded(reloadCells: reload)
            }
        }
        self.updateSectionIndex()
        tableView.endUpdates()
    }
    
    private func updateSectionIndex() {
        var visibleSectionIndex = 0
        for section in allSections() {
            let visible = section.visibilityHandler?() ?? true
            if !visible {
                section.index = nil
            }else{
                section.index = visibleSectionIndex
                visibleSectionIndex += 1
            }
        }
    }
    
    func commitSection(section: SectionProvider, at: Int? = nil) {
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        let index = at ?? allSections().count
        insertSection(section, at: index)
        updateSectionIndex()
        if let sectionIndex = section.index {
            let indexSet = NSIndexSet(index: sectionIndex)
            tableView.insertSections(indexSet as IndexSet, with: UITableViewRowAnimation.fade)
        }
        section.configHandler?(section)
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }

    
}
