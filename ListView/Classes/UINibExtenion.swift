//
//  UINibExtenion.swift
//  Pods
//
//  Created by mohsen shakiba on 12/30/1395 AP.
//
//

import Foundation
import UIKit

extension UINib {
    
    static func fromType(type: AnyClass) -> UINib {
        let nibName = name(type: type)
        return UINib(nibName: nibName, bundle: nil)
    }
    
    static func name(type: AnyClass) -> String {
        let nibName = NSStringFromClass(type).components(separatedBy: ".").last
        return nibName!
    }
    
}
