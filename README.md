# ListView

[![CI Status](http://img.shields.io/travis/mohsen shakiba/ListView.svg?style=flat)](https://travis-ci.org/mohsen shakiba/ListView)
[![Version](https://img.shields.io/cocoapods/v/ListView.svg?style=flat)](http://cocoapods.org/pods/ListView)
[![License](https://img.shields.io/cocoapods/l/ListView.svg?style=flat)](http://cocoapods.org/pods/ListView)
[![Platform](https://img.shields.io/cocoapods/p/ListView.svg?style=flat)](http://cocoapods.org/pods/ListView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ListView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ListView"
```

## Author

mohsen shakiba, work.shakiba@gmail.com

## License

ListView is available under the MIT license. See the LICENSE file for more info.
